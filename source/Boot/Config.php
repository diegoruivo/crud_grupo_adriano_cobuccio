<?php
/**
 * DATABASE
 */
define("CONF_DB_HOST", "localhost");
define("CONF_DB_USER", "ruivo543_crud");
define("CONF_DB_PASS", "crudroot");
define("CONF_DB_NAME", "ruivo543_crud");

/**
 * PROJECT URLs
 */
define("CONF_URL_BASE", "https://www.ruivooffice.com.br/crud_grupo_adriano_cobuccio");
define("CONF_URL_TEST", "https://www.localhost/crud_grupo_adriano_cobuccio");


/**
 * SITE
 */
define("CONF_SITE_NAME", "Teste CRUD Diego Ruivo para o Grupo Adriano Cobuccio");
define("CONF_SITE_TITLE", "Teste CRUD Diego Ruivo para o Grupo Adriano Cobuccio");
define("CONF_SITE_DESC", "Teste CRUD Diego Ruivo para o Grupo Adriano Cobuccio");
define("CONF_SITE_LANG", "pt_BR");
define("CONF_SITE_DOMAIN", "ruivooffice.com.br");
define("CONF_SITE_ADDR_STREET", "Rua Newton Rosa");
define("CONF_SITE_ADDR_NUMBER", "81");
define("CONF_SITE_ADDR_COMPLEMENT", "");
define("CONF_SITE_ADDR_CITY", "Itapeva");
define("CONF_SITE_ADDR_STATE", "SP");
define("CONF_SITE_ADDR_ZIPCODE", "18410-230");

/**
 * DATES
 */
define("CONF_DATE_BR", "d/m/Y H:i:s");
define("CONF_DATE_APP", "Y-m-d H:i:s");

/**
 * PASSWORD
 */
define("CONF_PASSWD_MIN_LEN", 8);
define("CONF_PASSWD_MAX_LEN", 40);
define("CONF_PASSWD_ALGO", PASSWORD_DEFAULT);
define("CONF_PASSWD_OPTION", ["cost" => 10]);

/**
 * MESSAGE
 */
define("CONF_MESSAGE_CLASS", "message");
define("CONF_MESSAGE_INFO", "info icon-info");
define("CONF_MESSAGE_SUCCESS", "success icon-check-square-o");
define("CONF_MESSAGE_WARNING", "warning icon-warning");
define("CONF_MESSAGE_ERROR", "error icon-warning");

/**
 * VIEW
 */
define("CONF_VIEW_PATH", __DIR__ . "/../../shared/views");
define("CONF_VIEW_EXT", "php");
define("CONF_VIEW_THEME", "crud_grupo_adriano_cobuccio");
define("CONF_VIEW_ADMIN", "admin_crud_grupo_adriano_cobuccio");

/**
 * UPLOAD
 */
define("CONF_UPLOAD_DIR", "storage");
define("CONF_UPLOAD_IMAGE_DIR", "images");
define("CONF_UPLOAD_FILE_DIR", "files");
define("CONF_UPLOAD_MEDIA_DIR", "medias");

/**
 * IMAGES
 */
define("CONF_IMAGE_CACHE", CONF_UPLOAD_DIR . "/" . CONF_UPLOAD_IMAGE_DIR . "/cache");
define("CONF_IMAGE_SIZE", 2000);
define("CONF_IMAGE_QUALITY", ["jpg" => 75, "png" => 5]);

/**
 * MAIL
 */
define("CONF_MAIL_HOST", "mail.ruivooffice.com.br");
define("CONF_MAIL_PORT", "587");
define("CONF_MAIL_USER", "contato@ruivooffice.com.br");
define("CONF_MAIL_PASS", "3sc@mb0000");
define("CONF_MAIL_SENDER", ["name" => "Diego Ruivo", "address" => "contato@ruivooffice.com.br"]);
define("CONF_MAIL_OPTION_LANG", "br");
define("CONF_MAIL_OPTION_HTML", true);
define("CONF_MAIL_OPTION_AUTH", true);
define("CONF_MAIL_OPTION_SECURE", "ssl");
define("CONF_MAIL_OPTION_CHARSET", "utf-8");



/**
 * SOCIAL
 */
define("CONF_SOCIAL_TWITTER_CREATOR", "@diegoeruivo");
define("CONF_SOCIAL_TWITTER_PUBLISHER", "@diegoeruivo");
define("CONF_SOCIAL_FACEBOOK_APP", "diegoeruivo");
define("CONF_SOCIAL_FACEBOOK_PAGE", "diegoeruivo");
define("CONF_SOCIAL_FACEBOOK_AUTHOR", "diegoeruivo");
define("CONF_SOCIAL_GOOGLE_PAGE", "diegoeruivo");
define("CONF_SOCIAL_GOOGLE_AUTHOR", "diegoeruivo");