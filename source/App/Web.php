<?php

namespace Source\App;

use Source\Core\Connect;
use Source\Core\Controller;
use Source\Models\Auth;
use Source\Models\Category;
use Source\Models\Post;
use Source\Models\Report\Access;
use Source\Models\Report\Online;
use Source\Models\User;
use Source\Support\Pager;

class Web extends Controller
{
    /**
     * Web Constructor
     */
    public function __construct() 
    {
        parent::__construct( __DIR__. "/../../themes/" . CONF_VIEW_THEME . "/");

        

    }

    /**
     * SITE HOME
     */
    public function home(): void
    {
        
        $head = $this->seo->render(
            CONF_SITE_NAME . "-" . CONF_SITE_TITLE,
            "CONF_SITE_DESC",
            url(),
            url("/assets/images/share.jpg")
        );
        
        echo $this->view->render("home", [
            "head" => $head,
            "video" => "Ij3voQLxeu0",
            "blog" => (new Post())->find()->order("post_at DESC")->limit(6)->fetch(true)
        ]);
    }

    /**
     * SITE SOBRE
     */
    public function about(): void
    {

     
        $head = $this->seo->render(
            "Conheça o " . CONF_SITE_NAME . "-" . CONF_SITE_DESC,
            "CONF_SITE_DESC",
            url("/sobre"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("about", [
            "head" => $head,
            "video" => "Ij3voQLxeu0"
        ]);
    }

    /**
     * SITE BLOG
     * @param array|null $data
     */
    public function blog(?array $data): void
    {
        $head = $this->seo->render(
            "Blog - " . CONF_SITE_NAME,
            "Confira em nosso blog dicas e sacadas de como controlar melhorar suas contas.",
            url("/blog"),
            theme("/assets/images/share.jpg")
        );

        $blog = (new Post())->find();
        $pager = new Pager(url("/blog/p/"));
        $pager->pager($blog->count(), 9, ($data['page'] ?? 1));

        echo $this->view->render("blog", [
            "head" => $head,
            "blog" => $blog->limit($pager->limit())->offset($pager->offset())->fetch(true),
            // "blog" => $blog->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE BLOG CATEGORY
     * @param array $data
     */
    public function blogCategory(array $data): void
    {
        $categoryUri = filter_var($data["category"]);
        $category = (new Category())->findByUri($categoryUri);

        if (!$category) {
            redirect("/blog");
        }

        $blogCategory = (new Post())->findPost("category = :c", "c={$category->id}");
        $page = (!empty($data['page']) && filter_var($data['page'], FILTER_VALIDATE_INT) >= 1 ? $data['page'] : 1);
        $pager = new Pager(url("/blog/em/{$category->uri}/"));
        $pager->pager($blogCategory->count(), 9, $page);

        $head = $this->seo->render(
            "Artigos em {$category->title} - " . CONF_SITE_NAME,
            $category->description,
            url("/blog/em/{$category->uri}/{$page}"),
            ($category->cover ? image($category->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        echo $this->view->render("blog", [
            "head" => $head,
            "title" => "Artigos em {$category->title}",
            "desc" => $category->description,
            "blog" => $blogCategory
                ->limit($pager->limit())
                ->offset($pager->offset())
                ->order("post_at DESC")
                ->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE BLOG SEARCH
     * @param array $data
     */
    public function blogSearch(array $data): void
    {
        if (!empty($data['s'])) {
            $search = str_search($data['s']);
            echo json_encode(["redirect" => url("/blog/buscar/{$search}/1")]);
            return;
        }

        $search = str_search($data['search']);
        $page = (filter_var($data['page'], FILTER_VALIDATE_INT) >= 1 ? $data['page'] : 1);

        if ($search == "all") {
            redirect("/blog");
        }

        $head = $this->seo->render(
            "Pesquisa por {$search} - " . CONF_SITE_NAME,
            "Confira os resultados de sua pesquisa para {$search}",
            url("/blog/buscar/{$search}/{$page}"),
            theme("/assets/images/share.jpg")
        );

        $blogSearch = (new Post())->findPost("MATCH(title, subtitle) AGAINST(:s)", "s={$search}");

        if (!$blogSearch->count()) {
            echo $this->view->render("blog", [
                "head" => $head,
                "title" => "PESQUISA POR:",
                "search" => $search
            ]);
            return;
        }

        $pager = new Pager(url("/blog/buscar/{$search}/"));
        $pager->pager($blogSearch->count(), 9, $page);

        echo $this->view->render("blog", [
            "head" => $head,
            "title" => "PESQUISA POR:",
            "search" => $search,
            "blog" => $blogSearch->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE BLOG POST
     * @param array $data
     */
    public function blogPost(array $data): void
    {
        $post = (new Post())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            $post->subtitle,
            url("/blog/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        echo $this->view->render("blog-post", [
            "head" => $head,
            "post" => $post,
            "related" => (new Post())
                ->findPost("category = :c AND id != :i", "c={$post->category}&i={$post->id}")
                ->order("rand()")
                ->limit(3)
                ->fetch(true)
        ]);
    }

   /**
     * SITE REGISTER
     * @param null|array $data
     */
    public function register(?array $data): void
    {

        if(!empty($data['csrf'])) {

            if(!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar o formulário.")->render();
                echo json_encode($json);
                return;
            }
       
        if(in_array("", $data)) {
            $json['message'] = $this->message->info("Informe seus dados para criar conta.")->render();
            return;
        }

        $auth = new Auth();
        $user = new User();
        $user->bootstrap(
            $data["first_name"],
            $data["last_name"],
            $data["email"],
            $data["password"]
            
        );

        if ($auth->register($user)) {       
            
            // UPDATE LEVEL E STATUS
            $userUpdate = (new User())->findByEmail($data["email"]);
            if ($userUpdate && $userUpdate->status != "confirmed") {
                $userUpdate->level = "5";
                $userUpdate->status = "confirmed";
                $userUpdate->save();
            }

            $json['redirect'] = url("/admin/login");
        } else {
            $json['message'] = $auth->message()->render();
        }

            echo json_encode($json);
            return;

    }

        $head = $this->seo->render(
            "Cadastro " . CONF_SITE_NAME . "-" . CONF_SITE_DESC,
            "CONF_SITE_DESC",
            url("/cadastrar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-register", [
            "head" => $head
        ]);
    }




    /**
     * SITE NAV ERROR
     */
    public function error(array $data): void
    {
        
        $error = new \stdClass();

        switch($data['errcode']) {
            case "problemas":
                $error->code = "OPS";
                $error->title = "Estamos enfrentando problemas.";
                $error->message = "Nosso serviço não está diponível no momento.";
                $error->linkTitle = "ENVIAR E-MAIL";
                $error->link = "mailto:".CONF_MAIL_SENDER;
                break;

            case "manutencao":
                $error->code = "OPS";
                $error->title = "Estamos em manutenção.";
                $error->message = "Nosso serviço não está diponível no momento.";
                $error->linkTitle = null;
                $error->link = null;
                break;

            default:
            $error->code = $data['errcode'];
            $error->title = "Ooops. Conteúdo indisponivel";
            $error->message = "Conteúdo não existe.";
            $error->linkTitle = "Continue navegando";
            $error->link = url_back();
            break;

        }

        $head = $this->seo->render(
            "{$error->code} | {$error->title}",
            $error->message,
            url("/ops/{$error->code}"),
            theme("/assets/images/share.jpg"),
            false
        );
        
        echo $this->view->render("error", [
            "head" => $head,
            "error" => $error
        ]);
    }
}