<?php $v->layout("_theme"); ?>

<section class="about_page">
    <div class="about_page_content content">
        <header class="about_header">
            <h1>Teste PHP Crud</h1>
            <p>Teste CRUD Diego Ruivo para o Grupo Adriano Cobuccio</p>
        </header>

        <!--FEATURES-->
        <div class="about_page_steps">
            <article class="radius">
                <header>
                    <span class="icon icon-check-square-o icon-notext"></span>
                    <h3>Cadastre-se para começar</h3>
                    <p>Basta informar seus dados e confirmar seu cadastro para começar a usar GRATUITAMENTE os recursos
                        do Blog.</p>
                </header>
            </article>

            <article class="radius">
                <header>
                    <span class="icon icon-leanpub icon-notext"></span>
                    <h3>Lance seus Artigos</h3>
                    <p>Cadastre seus artigos em uma interface
                        simples e muito intuitiva.</p>
                </header>
            </article>

            <article class="radius">
                <header>
                    <span class="icon icon-coffee icon-notext"></span>
                    <h3>Divulgue</h3>
                    <p>As automações do Blog permitem você compartilhar seus artigos nas redes sociais.</p>
                </header>
            </article>
        </div>
    </div>

    <div class="about_page_media">
        <div class="about_media_video">
            <div class="embed">
                <iframe width="560" height="315"
                        src="https://www.youtube.com/embed/<?= $video; ?>?rel=0&amp;showinfo=0" frameborder="0"
                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <aside class="about_page_cta">
        <div class="about_page_cta_content container content">
            <h2>Ainda não está usando o Blog?</h2>
            <p>Com ele você tem todos os recursos necessários para controlar seus artigo. Crie sua conta e comece a
                agora! É simples, fácil e gratuito...</p>
            <a href="<?= url("/cadastrar"); ?>" title="Cadastre-se"
               class="about_page_cta_btn transition radius icon-check-square-o">Quero Testar</a>
        </div>
    </aside>
</section>
