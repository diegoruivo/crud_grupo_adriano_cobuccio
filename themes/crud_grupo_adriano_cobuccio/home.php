<?php $v->layout("_theme"); ?>

    <!--FEATURED-->
    <article class="home_featured">
        <div class="home_featured_content container content">
            <header class="home_featured_header">
                <h1>Teste PHP CRUD PDO MVC</h1>
                <p>Diego Neves Ruivo para o Grupo Adriano Cobuccio</p>
                <p><a href="<?= url("/cadastrar"); ?>"
               class="footer_optout_btn gradient gradient-green gradient-hover radius icon-check-square-o">Quero
                Testar</a></p>
                <p class="features">Desde já agardeço a oportunidade.</p>
            </header>
        </div>

        <div class="home_featured_app">
            <img src="<?= theme("/assets/images/home-app.jpg"); ?>" alt="CafeControl" title="CafeControl"/>
        </div>
    </article>

    
    <!--VIDEO-->
    <article class="home_video">
        <div class="home_video_content container content">
            <header>
                <h2>Conheça sobre Ruivo Office</h2>
                <span data-modal=".home_video_modal" class="icon-play-circle-o icon-notext transition"></span>
            </header>
        </div>

        <div class="home_video_modal j_modal_close">
            <div class="home_video_modal_box">
                <div class="embed">
                    <iframe width="560" height="315"
                            src="https://www.youtube.com/embed/<?= $video; ?>?rel=0&amp;showinfo=0"
                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </article>

    <!--BLOG-->
<?php if (empty($blog)): ?>
    <div class="content content">
        <div class="empty_content">
            <img class="empty_content_cover" title="Empty Content" alt="Empty Content"
                 src="<?= theme("/assets/images/empty-content.jpg"); ?>"/>
            <h3 class="empty_content_title">Estamos trabalhando aqui</h3>
            <p class="empty_content_desc">Em breve você terá dicas e sacadas sobre como controlar melhor suas contas
                :)</p>
        </div>
    </div>
<?php else: ?>
    <section class="blog">
        <div class="blog_content container content">
            <header class="blog_header">
                <h2>Nossos artigos</h2>
                <p>Confira nossas dicas!</p>
            </header>

            <div class="blog_articles">
                <?php foreach ($blog as $post): ?>
                    <?php $v->insert("blog-list", ["post" => $post]); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>