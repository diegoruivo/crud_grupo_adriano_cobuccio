<article class="blog_article">
    <a title="<?= $post->title ?>" href="<?= url("/blog/{$post->uri}"); ?>">
        <img title="<?= $post->title ?>" alt="<?= $post->title ?>" src="<?= image($post->cover, 600, 340); ?>"/>
    </a>
    <header>
        <p class="meta"><?= $post->category()->title ?> &bull; Por <?= $post->author()->first_name ?> &bull; <?= date_fmt($post->post_at) ?></p>
        <h2><a title="Post" href="<?= url("/blog/titulo-post"); ?>"><?= $post->title ?></a></h2>
        <p><a title="Post" href="<?= url("/blog/titulo-post"); ?>"><?= str_limit_chars($post->subtitle, 120) ?></a></p>
    </header>
</article>